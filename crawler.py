import sys
import newspaper
from unidecode import unidecode

def generar_ulrs(query):
    urls = []
    for i in range(1,10):
        urls.append("http://www.eltiempo.com/buscar/" + str(i) + "?q=" + query)
    return urls

def obtener_enlaces(pagina):
    return newspaper.build(pagina, memoize_articles=False)
    
def manejar_articulos(articulos, query):
    articulos_seleccionados = []
    otros_articulos = []
    articulos_fallidos = []  

    for articulo in articulos.articles:
        try:
            articulo.download()
        except:
            print('No se ha encontrado: ' + articulo.url)
            articulos_fallidos.append(articulo)
        else:
            articulo.parse()
            q_limpio = unidecode(query).lower()
            a_limpio = unidecode(articulo.text).lower()
            t_limpio = unidecode(articulo.title).lower()
            u_limpia = unidecode(articulo.url).lower()

            if(q_limpio in a_limpio  or q_limpio in t_limpio or q_limpio in u_limpia):
                print('Artículo agregado a relacionados')
                print(articulo.url)
                print(articulo.meta_keywords)
                articulo.nlp()
                print(articulo.keywords)
                articulos_seleccionados.append(articulo)
            else:
                print('Artículo agregado a otros')
                otros_articulos.append(articulo)

manejar_articulos(obtener_enlaces(generar_ulrs(sys.argv[1])[0]), sys.argv[1])