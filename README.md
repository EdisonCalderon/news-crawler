# News Crawler - GESDATOS #

En este repositorio se encuentra el código para el crawler de noticias para el grupo GESDATOS

### ¿Para qué es este repositorio? ###

* Resúmen rápido
Se traen noticias relacionadas con un query dado, desde www.eltiempo.com

* Versión
0.0.1

### How do I get set up? ###

* Summary of set up
Se hace uso de Python3, pip3 y virtualenv (opcional)

* Dependencies
    - newspaper
    - unidecode

* Configuration
Se recomienda usar un virtualenv, para las dependiendencias:

    - crear virtual env (opcional):
    ```bash
    $ pip3 install virtualenv
    (debemos estar en el directorio del proyecto)
    $ virtualenv python
    $ cd news-crawler
    $ source python/bin/activate (cambiamos al virtualenv)
    ```
    - instalamos las dependencias

    ```bash
    $ pip3 install -r requirements.txt
    ```

    - Correr el script

    ```bash
    $ pyton3 crawler.py "query"
    ```

### Retos  ###

* Inclusión de nuevos sitios
* Mejora de la heurística de selección de artículos (precisión/rendimiento)

### Autores ###

* Edison Calderón
* Leonardo Dallos
* Guillermo Cotta
